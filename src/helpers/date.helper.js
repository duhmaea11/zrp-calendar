export const getStartEndWeek = (date = new Date()) => {
  const first = date.getDate() - date.getDay();
  const last = first + 6;

  const firsDay = new Date(date.setDate(first));
  let lastDay = new Date(date.setDate(last));

  if(firsDay > lastDay) {
    const currentMonth = lastDay.getMonth()
    lastDay = lastDay.setMonth(currentMonth + 1)
  }

  return [firsDay, lastDay];
};

export const formatDate = (date = new Date()) =>
  date.toLocaleDateString("pt-BR");
