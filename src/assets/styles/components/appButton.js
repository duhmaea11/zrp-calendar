import styled from "styled-components";

const colorDisct = {
  primary: "#9CC763",
  secondary: "#EC605F",
};

export const AppButton = styled.button`
  color: #fff;
  border: none;
  padding: 9px 35px;
  border-radius: 30px;
  cursor: pointer;

  background: ${({ mode, bgColor }) => {
    const colorDefault = colorDisct.primary;

    if (!bgColor) return colorDisct[mode] || colorDefault;
    return bgColor || colorDefault;
  }};
`;
