import styled from "styled-components";

const bgPrimary = "#232323";

export const AppContainer = styled.div`
  background: ${({ bgColor }) => bgColor || bgPrimary};
  width: 100%;
  color: #fff;
`;
