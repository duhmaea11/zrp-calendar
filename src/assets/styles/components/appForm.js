import styled from "styled-components";

export const AppFormGroup = styled.div`
  width: ${({ width }) => width || "auto"};
  margin-bottom: 1rem;

  > label {
    display: block;
    margin-bottom: 8px;
  }

  > input,
  > select,
  > textarea {
    width: calc(100% - 44px);
    display: block;
    border-radius: 38px;
    padding: 9px 22px;
    background: transparent;
    color: #ffff;

    border-width: 1px;
    border-style: solid;
    border-color: ${({ invalid }) => invalid ? "#cf0000" : "#FFF"};

    &:focus {
      outline: none !important;
      border-color: ${({ invalid }) => invalid ? "#cf0000" : "#FFF"};
    }
  }
`;

export const FormControlError = styled.span`
  color: #cf0000;
  font-size: 14px;
  font-weight: 400;
  letter-spacing: 0em;
  white-space: nowrap;
`;
