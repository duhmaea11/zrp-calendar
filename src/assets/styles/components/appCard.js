import styled from "styled-components";

const colorDisct = {
  primary: "#F6A848",
  secondary: "#8E6C97",
};

export const AppCard = styled.div`
  background: ${({ bgColor }) => {
    return bgColor || colorDisct.primary;
  }};
`;
