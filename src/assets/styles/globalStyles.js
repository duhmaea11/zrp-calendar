import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    * { 
        margin: 0; 
        padding: 0; 
    }

    html {
        height: 100%;
    }

    body {
        font-family: 'Montserrat', sans-serif;
    }

    #root {
        display: flex;
        min-height: 100vh;
    }

    ::-webkit-calendar-picker-indicator {
        filter: invert(1);
    }
`;

export default GlobalStyle;
