import { createSlice } from "@reduxjs/toolkit";
import { guid } from "../helpers/guid.helper";

const getState = (prop) => {
  const valueStringfy = JSON.stringify(prop);
  return JSON.parse(valueStringfy);
};

const event = createSlice({
  name: "event",
  initialState: {
    events: [],
    currentEvent: null,
  },
  reducers: {
    addEvent(state, event) {
      const eventDTO = event;
      eventDTO.payload.id = guid();

      state.events.push(eventDTO);
    },
    removeEvent(state, { payload: eventId }) {
      const events = getState(state.events);

      const filteredEvents = events.filter(
        ({ payload }) => payload.id !== eventId
      );
      state.events = filteredEvents;
    },
    updateEvent(state, event) {
      const events = getState(state.events);
      const currentEvent = events.find(
        ({ payload }) => payload.id === event.payload.id
      );

      const indexCurrentEvent = events.indexOf(currentEvent);
      events[indexCurrentEvent] = event

      state.events = events
    },
    setCurrentEvent(state, event) {
      state.currentEvent = event;
    },
    clearCurrentEvent(state) {
      const currentEvent = getState(state.currentEvent);

      if(!currentEvent) return
      currentEvent.payload = null;

      state.currentEvent = currentEvent;
    },
  },
});

export const {
  addEvent,
  removeEvent,
  updateEvent,
  setCurrentEvent,
  clearCurrentEvent,
} = event.actions;

export default event.reducer;
