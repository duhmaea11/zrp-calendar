import React from "react";
import { Image, Container, Title, Content} from "./styles";
import imageLogo from "../../assets/images/Logo.svg";

const Logo = () => {
  return (
    <Container>
      <Image src={imageLogo} />
      <Title>Calendar</Title>
    </Container>
  );
};

export default Logo;
