import styled from "styled-components";

export const Container = styled.article`
  display: flex;
`;

export const Image = styled.img`
  width: 128px;
  height: 60px;
  object-fit: none;
  user-select: none;
`;

export const Title = styled.h1`
  margin-left: 12px;
  font-size: 54px;
  font-weight: 700;
  letter-spacing: 0em;
  color: #ec605f;
  position: relative;
  top: 2px;
  user-select: none;
`;
