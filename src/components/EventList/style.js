import styled from "styled-components";

export const Container = styled.section`
  display: flex;
  min-height: 20vh;
  margin-bottom: 22px;

  @media (max-width: 880px) {
    flex-direction: column;
  }
`;

export const ContainerTitle = styled.div`
  width: 115px;
  padding: 6px 20px 0px 0px;

  @media (max-width: 880px) {
    padding: 0px;
    margin-bottom: 10px;
  }
`;

export const Title = styled.h4`
  text-align: right;
  font-size: 14px;
  font-weight: bold;

  @media (max-width: 880px) {
    text-align: left;
  }
`;

export const EmptyMessage = styled.span`
  flex: 1;
  color: #ec605f;
  font-size: 14px;
  font-weight: 700;
  text-align: center;
  letter-spacing: 0em;
  padding: 6px 20px 0px 0px;
`;

export const Grid = styled.article`
  flex: 1;
  flex-wrap: wrap;
  display: flex;

  > div {
    margin-bottom: 10px;
    margin-right: 10px;

    @media (max-width: 880px) {
      width: 100%;
    }
  }
`;
