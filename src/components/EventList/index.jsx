import React from "react";
import { formatDate } from "../../helpers/date.helper";
import Card from "../Card";
import { Container, ContainerTitle, Title, Grid, EmptyMessage } from "./style";

const EventList = (props) => {
  const { title, events, colorCard, emptyMessage, onDelete, onEdit } = props;

  const handleDelete = (id) => onDelete(id);
  const handleEdit = (id) => onEdit(id);

  const dateEventLabel = ({ date, time }) => {
    const dateParse = formatDate(new Date(date));
    return `${dateParse} ${time}`;
  };
  return (
    <Container>
      <ContainerTitle>
        <Title>{title}</Title>
      </ContainerTitle>

      <Grid>
        {events.length ? (
          events.map((card, index) => (
            <Card
              key={index}
              title={card.name}
              description={card.local}
              subtitle={dateEventLabel(card)}
              bgColor={colorCard}
              onDelete={() => handleDelete(card)}
              onEdit={() => handleEdit(card)}
            />
          ))
        ) : (
          <EmptyMessage>{emptyMessage}</EmptyMessage>
        )}
      </Grid>
    </Container>
  );
};

export default EventList;
