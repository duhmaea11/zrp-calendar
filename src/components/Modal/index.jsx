import React from "react";

import {
  ContainerModal,
  AppCard,
  Title,
  Text,
  TextDescription,
  Button,
  ButtonGroup,
} from "./style";

const Modal = (props) => {
  const { show, onAction, cardProp } = props;

  const closeModal = (result = false) => {
    console.log("????");
    onAction(result);
  };

  return show ? (
    <ContainerModal onClick={() => closeModal()}>
      <AppCard bgColor="#EC605F">
        <Title>remover este evento?</Title>

        <Text>{cardProp.title}</Text>
        <Text style={{ margin: "10px 0px" }}>{cardProp.subtitle}</Text>
        <TextDescription>{cardProp.description}</TextDescription>

        <ButtonGroup>
          <Button onClick={() => closeModal()}>Cancelar</Button>
          <Button onClick={() => closeModal(true)}>Excluir</Button>
        </ButtonGroup>
      </AppCard>
    </ContainerModal>
  ) : null;
};

export default Modal;
