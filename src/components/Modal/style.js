import styled from "styled-components";
import { AppButton, AppCard as Card } from "../../assets/styles/components";

export const ContainerModal = styled.section`
  top: 0;
  left: 0;
  z-index: 20;
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;

  position: fixed;
  background: #00000094;
`;

export const AppCard = styled(Card)`
  height: 239px;
  width: 370.23px;
  padding: 14px 14px 0px 14px;
  border-radius: 4px;
  box-sizing: border;
  user-select: none;
  display: flex;
  flex-direction: column;

  @media (max-width: 576px) {
    width: 70vw;
  }
`;

export const Title = styled.b`
  font-size: 24px;
  margin-bottom: 30px;
`;

export const Text = styled.p``;

export const TextDescription = styled(Text)`
  overflow: hidden;
  display: -webkit-box;
  text-overflow: ellipsis;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

export const ButtonGroup = styled.article`
  width: 100%;
  display: flex;
  margin-top: auto;
  padding-bottom: 25px;
  justify-content: space-around;
`;

export const Button = styled(AppButton)`
  background: #6799cc;
  width: 114px;
  text-align: center;
  padding-left: 0px;
  padding-right: 0px;

  @media (max-width: 576px) {
    width: calc(50% - 12px);
  }
`;
