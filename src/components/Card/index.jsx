import React from "react";
import {
  AppCard,
  Title,
  Text,
  TextDescription,
  Button,
  ButtonGroup,
  Icon,
} from "./style";

import Delete from "../../assets/icons/Delete.svg";
import Edit from "../../assets/icons/Edit.svg";

const Card = (props) => {
  const { bgColor, title, description, subtitle, onEdit, onDelete } = props;

  return (
    <AppCard bgColor={bgColor}>
      <Title>{title}</Title>
      <Text style={{ margin: "10px 0px" }}>{subtitle}</Text>

      <TextDescription>{description}</TextDescription>

      <ButtonGroup>
        <Button onClick={onDelete}>
          <Icon src={Delete} />
        </Button>

        <Button onClick={onEdit}>
          <Icon src={Edit} />
        </Button>
      </ButtonGroup>
    </AppCard>
  );
};

export default Card;
