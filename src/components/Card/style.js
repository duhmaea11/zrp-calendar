import styled from "styled-components";
import { AppCard as Card } from "../../assets/styles/components/appCard";

export const AppCard = styled(Card)`
  height: 162px;
  width: 242.5px;
  padding: 14px 14px 0px 14px;
  border-radius: 4px;
  box-sizing: border;
  user-select: none;
  display: flex;
  flex-direction: column;
`;

export const Title = styled.b`
  font-size: 14px;
`;

export const Text = styled.p``;

export const TextDescription = styled(Text)`
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;

export const Button = styled.button`
  background: transparent;
  border: none;
  padding: 10px;
  cursor: pointer;

  &:first-of-type {
    margin-right: 8px;
  }
`;

export const ButtonGroup = styled.article`
  display: flex;
  width: 100%;
  justify-content: end;
  margin-top: auto;
`;

export const Icon = styled.img`
  width: 20px;
`;
