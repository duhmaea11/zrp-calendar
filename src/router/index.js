import React from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import EventRegister from "../pages/EventRegister";
import Home from "../pages/Home";

const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/event-register" element={<EventRegister />}></Route>
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
