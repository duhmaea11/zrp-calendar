import styled from "styled-components";
import {
  AppContainer,
  AppButton,
  AppFormGroup,
  FormControlError
} from "../../assets/styles/components";

export const Container = styled(AppContainer)`
  display: flex;
  padding-top: 3rem;
  justify-content: center;

  @media (max-width: 880px) {
    padding-left: 22px;
    padding-right: 22px;
  }
`;

export const Button = styled(AppButton)`
  width: calc(50% - 12px);
`;

export const Form = styled.form`
  width: 34%;

  @media (max-width: 880px) {
    width: 100%;
  }
`;

export const FormGroup = styled(AppFormGroup)``;

export const Label = styled.label``;

export const Input = styled.input``;

export const Title = styled.h3`
  font-size: 24px;
  font-weight: 700;
  letter-spacing: 0em;
  margin-bottom: 22px;
`;

export const ButtonGroup = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const Span = styled(FormControlError)``;