import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import {
  addEvent,
  updateEvent,
  clearCurrentEvent,
} from "../../store/Event.store";
import { yupResolver } from "@hookform/resolvers/yup";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";

import {
  Container,
  Button,
  FormGroup,
  Form,
  Input,
  Label,
  Title,
  ButtonGroup,
  Span,
} from "./styles";

const validationEvent = yup.object().shape({
  name: yup.string().required("Insira um nome."),
  local: yup.string().required("Insira um local."),
  date: yup.string().required("Insira uma data."),
  time: yup.string().required("Insira um horário."),
});

const EventRegister = (prop) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [isNew, setIsNew] = useState(true);
  const currentEvent = useSelector(({ event }) => event.currentEvent?.payload);

  const {
    register,
    setValue,
    handleSubmit,
    formState: { errors },
  } = useForm({ resolver: yupResolver(validationEvent) });

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    if (currentEvent) {
      setFormValue();
      setIsNew(false);
    }
  });

  const setFormValue = () => {
    setValue("name", currentEvent.name);
    setValue("local", currentEvent.local);
    setValue("data", currentEvent.date);
    setValue("time", currentEvent.time);
  };

  const backToHome = () => {
    dispatch(clearCurrentEvent());
    navigate("/");
  };

  const saveEvent = (event) => (!isNew ? editEvent(event) : createEvent(event));

  const createEvent = (event) => {
    try {
      dispatch(addEvent(event));
      backToHome();
    } catch (error) {
      console.error(error);
    }
  };

  const editEvent = (event) => {
    try {
      const eventDTO = { ...event, id: currentEvent.id };
      dispatch(updateEvent(eventDTO));

      backToHome();
    } catch (error) {
      console.error(error);
    }
  };

  const errorMessage = (field) => errors[field]?.message || "";

  return (
    <Container bgColor="#8E6C97">
      <Form onSubmit={handleSubmit(saveEvent)} autoComplete="off">
        <Title>{isNew ? "Criar novo" : "Editar"} evento ZRPenho</Title>

        <FormGroup invalid={errorMessage("name")}>
          <Label>nome do evento</Label>
          <Input
            type="text"
            name="name"
            autoComplete="off"
            {...register("name")}
          />
          <Span>{errorMessage("name")}</Span>
        </FormGroup>

        <FormGroup invalid={errorMessage("local")}>
          <Label>local</Label>
          <Input type="text" name="local" {...register("local")} />
          <Span>{errorMessage("local")}</Span>
        </FormGroup>

        <FormGroup width="55%" invalid={errorMessage("date")}>
          <Label>data</Label>
          <Input
            type="date"
            name="date"
            {...register("date", { value: currentEvent?.date || new Date() })}
          />
          <Span>{errorMessage("date")}</Span>
        </FormGroup>

        <FormGroup width="38%" invalid={errorMessage("time")}>
          <Label>horário</Label>
          <Input type="time" name="time" {...register("time")} />
          <Span>{errorMessage("time")}</Span>
        </FormGroup>

        <ButtonGroup>
          <Button type="button" onClick={() => backToHome()}>
            cancelar
          </Button>
          <Button type="submit">{isNew ? "criar" : "atualizar"} evento</Button>
        </ButtonGroup>
      </Form>
    </Container>
  );
};

export default EventRegister;
