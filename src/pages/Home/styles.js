import styled from "styled-components";
import { AppContainer, AppButton } from "../../assets/styles/components";

export const Container = styled(AppContainer)`
  padding: 60px 90px;
`;

export const LogoContainer = styled.div`
  @media (max-width: 880px) {
    display: flex;
    justify-content: center;

    img {
      width: 170px;
      height: 66px;
      object-fit: contain;
    }

    h1 {
      font-size: 24px;
      position: relative;
      right: 15%;
    }

    > article {
      align-items: center;
    }
  }
`;

export const Separator = styled.hr`
  background: #ffffff;
  margin: 35px 0px;
`;

export const Button = styled(AppButton)``;

export const Title = styled.h4`
  font-size: 14px;
  font-weight: 400;
  letter-spacing: 0em;
  margin: 35px 0px;
`;
