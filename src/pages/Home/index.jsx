import React, { useState } from "react";
import Logo from "../../components/Logo";
import { removeEvent, setCurrentEvent } from "../../store/Event.store";
import Modal from "../../components/Modal";

import { useNavigate } from "react-router-dom";
import EventList from "../../components/EventList";
import { Container, Title, Button, Separator, LogoContainer } from "./styles";
import { useDispatch, useSelector } from "react-redux";
import { formatDate, getStartEndWeek } from "../../helpers/date.helper";

const Home = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const events = useSelector(({ event }) =>
    event.events
      .map(({ payload }) => payload)
      .sort((a, b) => new Date(a.date) - new Date(b.date))
  );

  const [initial, end] = getStartEndWeek();
  const [openModal, setOpenModal] = useState(false);
  const [cardProp, setCardProp] = useState({});

  const eventsWeek = events.filter(({ date }) => {
    const dateEvent = new Date(date);
    return dateEvent >= initial && dateEvent <= end;
  });

  const nextEvents = events.filter(({ id, date }) => {
    const isWeek = !eventsWeek.some((item) => item.id === id);
    return isWeek && new Date(date) > end;
  });

  const goToRegister = (event) => {
    if (event) dispatch(setCurrentEvent(event));
    navigate(`/event-register`);
  };

  const onDeleteEvent = (event) => {
    const dateParse = formatDate(new Date(event.date));

    const cardPropDTO = {
      id: event.id,
      title: event.name,
      description: event.local,
      subtitle: `${dateParse} ${event.time}`,
    };

    setCardProp(cardPropDTO);
    setOpenModal(true);
  };

  const onCloseModal = () => {
    setCardProp({});
    setOpenModal(false);
  };

  const onActionModal = (confirm) => {
    console.log(confirm);
    if (!confirm) onCloseModal();
    else {
      dispatch(removeEvent(cardProp.id));
      onCloseModal();
    }
  };

  return (
    <Container>
      <LogoContainer>
        <Logo />
      </LogoContainer>

      <Title>Gerencie seus eventos aqui</Title>

      <Button onClick={() => goToRegister()}>Criar Evento</Button>
      <Separator />

      <EventList
        title="essa semana"
        events={eventsWeek}
        emptyMessage="não existem eventos registrados nessa semana"
        onDelete={(event) => onDeleteEvent(event)}
        onEdit={(event) => goToRegister(event)}
      />

      <EventList
        title="próximos eventos"
        events={nextEvents}
        emptyMessage="não existem eventos futuros registrados"
        colorCard="#8E6C97"
        onDelete={(event) => onDeleteEvent(event)}
        onEdit={(event) => goToRegister(event)}
      />

      <Modal
        show={openModal}
        cardProp={cardProp}
        closeModal={setOpenModal}
        onAction={(result) => onActionModal(result)}
      />
    </Container>
  );
};

export default Home;
