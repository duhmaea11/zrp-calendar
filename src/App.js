import React from "react";
import store from "./store";
import { Provider } from "react-redux";

import Routes from "./router";
import GlobalStyle from "./assets/styles/globalStyles";

import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
let persistor = persistStore(store);

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <GlobalStyle />
        <Routes />
      </PersistGate>
    </Provider>
  );
}

export default App;
